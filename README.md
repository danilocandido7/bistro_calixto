# README
Bistro Calixto

Things you may want to cover:

* Ruby version - 2.5.1

* Database - bistro_calixto_development (Postgres)

* Run tests: bundle exec rspec

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions
