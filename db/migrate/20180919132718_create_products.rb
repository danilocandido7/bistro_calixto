class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.date :purchase_date
      t.date :expiration_date
      t.string :brand
      t.decimal :amount
      t.decimal :price, :precision => 8, :scale => 2

      t.timestamps
    end
    add_index :products, :name
  end
end
