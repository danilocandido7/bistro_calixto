class CreateItemDishesAssociation < ActiveRecord::Migration[5.2]
  def change
    create_table :item_dishes do |t|
      t.belongs_to :dish,    index: true
      t.belongs_to :product, index: true
      t.decimal :amount
      t.string  :measure
    end
  end
end
