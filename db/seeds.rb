# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Product.destroy_all
[*1..60].each do |num|
  today = Time.now
  p = Product.create(name: "nome-#{num}", 
                 purchase_date: today - num.days, 
                 expiration_date: today + 1.days,
                 brand: 'brand',
                 amount: num, 
                 price: num,
                 measure: 2,
                 created_at: today - num.days
  )
  #p.valid?
  #puts p.errors.full_messages
end
