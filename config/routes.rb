Rails.application.routes.draw do
  root to: 'products#index'

  get 'admin' => 'admin#index'

  controller :sessions do
    get    'login'  => :new
    post   'login'  => :create
    delete 'logout' => :destroy
  end

  resources :products do
    get :cost, on: :collection
  end

  resources :dishes do
    get :sales, on: :collection
  end
  
  resources :users
end
