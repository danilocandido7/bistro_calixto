require 'rails_helper'

RSpec.describe ItemDish, type: :model do
  xcontext 'test' do
    it 'invalid' do
      product = create(:product, amount: -10)
      dish    = create(:dish)

      ItemDish.create(product: product, dish: dish)
      expect(dish.errors[:item_dishes].size).to eq 1
    end
  end
end
