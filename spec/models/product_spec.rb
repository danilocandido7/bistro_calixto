require 'rails_helper'

RSpec.describe Product, :type => :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:name).with_message('Nome não pode ficar em branco') }
    it { is_expected.to validate_presence_of(:measure).with_message('Medida não pode ficar em branco') }
    it { is_expected.to validate_presence_of(:brand).with_message('Marca não pode ficar em branco') }
    it { is_expected.to validate_presence_of(:amount).with_message('Quantidade não pode ficar em branco') }
  end

  context 'associations' do
    it { is_expected.to have_many(:dishes) }
  end

  context 'table fields' do
    it { is_expected.to have_db_column(:name).of_type(:string) }
    it { is_expected.to have_db_column(:measure).of_type(:string) }
    it { is_expected.to have_db_column(:brand).of_type(:string) }
    it { is_expected.to have_db_column(:amount).of_type(:decimal) }
    it { is_expected.to have_db_column(:expiration_date).of_type(:date) }
    it { is_expected.to have_db_column(:purchase_date).of_type(:date) }
  end

  context 'factories' do
    it { expect(build(:product)).to be_valid }
    it { expect(build(:product_expired)).to_not be_valid }
  end
end
