require 'rails_helper'

RSpec.describe Dish, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:name).with_message('Nome não pode ficar em branco') }
    it { is_expected.to validate_presence_of(:price).with_message('Preço deve ser maior que zero') }
    it { is_expected.to validate_numericality_of(:price).is_greater_than_or_equal_to(0) }
  end

  context 'associations' do
    it { is_expected.to have_many(:products) }
  end

  context 'table fields' do
    it { is_expected.to have_db_column(:name).of_type(:string) }
    it { is_expected.to have_db_column(:description).of_type(:text) }
    it { is_expected.to have_db_column(:price).of_type(:decimal) }
  end

  context 'factories' do
    it { expect(build(:dish)).to be_valid }
  end
end
