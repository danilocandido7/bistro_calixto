require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:name).with_message('Nome não pode ficar em branco') }
  end

  context 'table fields' do
    it { is_expected.to have_db_column(:name).of_type(:string) }
    it { is_expected.to have_db_column(:password_digest).of_type(:string) }
  end

  context 'factories' do
    it { expect(build(:user)).to be_valid }
  end
end
