FactoryBot.define do
  factory :dish do
    name { "Tapioca" }
    description { "Queijo e goma de tapioca" }
    price { 3.50 }
  end
end
