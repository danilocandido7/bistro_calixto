FactoryBot.define do
  factory :user do
    name { "Danilo Cândido" }
    password { BCrypt::Password.create('secret') }
  end
end
