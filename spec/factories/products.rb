FactoryBot.define do
  factory :product do
    name { "Queijo" }
    brand  { "Sadia" }
    amount { 10 }
    expiration_date { Time.now + 2.months }
    purchase_date { Time.now - 5.days }
    measure { 'gramas' }

    factory :product_expired do
      expiration_date { Time.now - 1.day }
    end
  end
end