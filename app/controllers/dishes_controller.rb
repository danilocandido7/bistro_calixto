class DishesController < ApplicationController
  before_action :set_dish, only: [:show, :edit, :update, :destroy]

  rescue_from 'ItemDish::Error' do |exception|
    redirect_to new_dish_path, notice: exception.message
  end

  def index
    @dishes = Dish.order('id DESC').page(params[:page])
  end

  def show
    fresh_when @dish
  end

  def new
    @dish = Dish.new
    @products = Product.available_amount
    if @products.empty?
      redirect_to new_product_path, notice: 'Primeiramente cadastre um produto.'
    end
  end

  def edit
    @products = Product.available_amount
  end

  def create
    @dish = Dish.new(dish_params)

    create_item_dishes
    if @dish.save
      redirect_to @dish, notice: 'Prato criado com sucesso.'
    else
      @products = Product.available_amount
      render :new
    end
  end

  def update
    create_item_dishes
    if @dish.update(dish_params)
      redirect_to @dish, notice: 'Prato atualizado com sucesso.'
    else
      render :edit
    end
  end

  def destroy
    @dish.destroy
    redirect_to dishes_url, notice: 'Prato deletado com sucesso.'
  end

  def sales
    if params[:search]
      search_date   = params[:search][:month]
      search_date ||= params[:search][:week]

      if search_date.present?
        start_date = end_date = Date.parse(search_date.gsub('-', '/'))
      end
    end

    start_date = end_date ||= Time.now

    week_relation  = Dish.where(created_at: start_date.beginning_of_week..end_date.end_of_week).select('price, count(id)').group('price, id')
    month_relation = Dish.where(created_at: start_date.beginning_of_month..end_date.end_of_month).select('price, count(id)').group('price, id')

    @week_costs = week_relation.sum(&:price)
    @week_size = week_relation.length

    @month_costs = month_relation.sum(&:price)
    @month_size = month_relation.length
  end

  private

  def set_dish
    @dish = Dish.find(params[:id])
  end

  def dish_params
    params.require(:dish).permit(:name, :description, :price)
  end

  def create_item_dishes
    return unless params[:item_dishes].present?

    amounts =  params[:item_dishes][:amount]

    amounts.each_with_index do |amount, index|
      product_id = params[:item_dishes][:product_id][index]
      measure = params[:item_dishes][:measure][index]
      @dish.item_dishes.build(product_id: product_id, amount: amount, measure: measure)
    end
  end
end
