class ApplicationController < ActionController::Base
  before_action :authorize, if: :exist_users?

  helper_method :current_user

  protected

  def authorize
    unless User.find_by(id: session[:user_id])
      redirect_to login_url, notice: 'Por favor, logue-se'
    end
  end

  private

  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def exist_users?
    User.exists?
  end
end
