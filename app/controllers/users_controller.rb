class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  rescue_from 'User::Error' do |exception|
    redirect_to users_url, notice: exception.message
  end

  def index
    @users = User.order(:name)
  end

  def show
    fresh_when @user
  end

  def new
    @user = User.new
  end

  def edit; end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to @user, notice: "Usuário #{@user.name} criado com sucesso."
    else
      render :new
    end
  end

  def update
    if @user.update(user_params)
      redirect_to @user, notice: "Usuário #{@user.name} atualizado com sucesso."
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to users_url, notice: 'Usuário deletado com sucesso.'
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :password, :password_confirmation)
  end
end
