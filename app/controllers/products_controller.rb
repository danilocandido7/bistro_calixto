class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    if params[:q]
      @products = Product.search(params[:q])
      render json: @products, status: :ok
    end
    @products = Product.order('id DESC').page(params[:page])
  end

  def show
    fresh_when @product
  end

  def new
    @product = Product.new
  end

  def edit; end

  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to @product, notice: 'Produto cadastrado sucesso'
    else
      render :new
    end
  end

  def update
    if @product.update(product_params)
      redirect_to @product, notice: 'Produto atualizado com sucesso'
    else
      render :edit
    end
  end

  def destroy
    @product.destroy
    redirect_to products_url, notice: 'Produto excluído com sucesso'
  end

  def cost
    if params[:search]
      search_date   = params[:search][:month]
      search_date ||= params[:search][:week]

      if search_date.present?
        start_date = end_date = Date.parse(search_date.gsub('-', '/'))
      end
    end

    start_date = end_date ||= Time.now

    week_relation  = Product.where(created_at: start_date.beginning_of_week..end_date.end_of_week).select('price, count(id)').group('price, id')
    month_relation = Product.where(created_at: start_date.beginning_of_month..end_date.end_of_month).select('price, count(id)').group('price, id')

    @week_costs = week_relation.sum(&:price)
    @week_size = week_relation.length

    @month_costs = month_relation.sum(&:price)
    @month_size = month_relation.length
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :purchase_date, :expiration_date, :brand, :amount, :price, :measure)
  end
end
