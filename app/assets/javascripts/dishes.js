$( document ).ready(function() {
  
  function include_new_element(id, name) {
    if($("[data-name_label="+id+"]").length == 0) {
      var div_dishes = $('#amount_dishes_selected');

      var label = "<label class='col-sm-2 col-form-label' data-name_label='"+id+"'>"+name+"</label>";
      var input_hidden = "<input class='form-control' value='"+id+"' type='hidden' name='item_dishes[product_id][]'>";
      var input = "<div class='col-sm-6'><input class='form-control' data-name_input='"+id+"' type='number' name='item_dishes[amount][]' required></div>"

      div_dishes.append(label);
      div_dishes.append(input_hidden);
      div_dishes.append(input);
      div_dishes.append(measure_options());
    }
  }

  function measure_options() {
    var options = "<select class='form-control col-sm-3' name='item_dishes[measure][]' id='product_measure'>" +
    "<option value='kilogramas'>kilogramas</option>" +
    "<option value='gramas'>gramas</option>" +
    "<option value='litros'>litros</option>" +
    "<option value='mililitros'>mililitros</option></select>"
    return options;
  }

  $( function() {
    $( "#dishes_name" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "/products",
          data: {
            q: request.term.toLowerCase()
          },
          success: function( data ) {
            var products = $.map( data, function( n ) {
              return { label: n.name, value: n.id };
            });
            response( products );
          }
        });
      },
      minLength: 3,
      select: function( event, ui ) {
        include_new_element(ui.item.value, ui.item.label);
        $(this).val(''); return false;
      },
      open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });
  });
});
