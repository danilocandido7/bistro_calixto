module ApplicationHelper
  BOOTSTRAP_ALERTS = { notice: :success, alert: :warning, error: :danger }.freeze

  def error_messages_for(object)
    render(:partial => 'application/error_messages', :locals => {object: object})
  end

  def flash_message
    messages = ''
    BOOTSTRAP_ALERTS.keys.each {|type|
      if flash[type]
        messages += "<div class='alert alert-#{BOOTSTRAP_ALERTS[type]}' role='alert'>#{flash[type]}</div>"
      end
    }

    messages.html_safe
  end
end
