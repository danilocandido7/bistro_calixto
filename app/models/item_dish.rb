class ItemDish < ApplicationRecord
  belongs_to :product
  belongs_to :dish

  delegate :name, to: :product, prefix: true

  before_save :subtract_from_products

  def name_with_amount_and_measure
    "#{product_name} - #{amount} (#{measure})"
  end

  class Error < StandardError
  end

  private

  def subtract_from_products
    if product.measure == self.measure
      product.amount -= self.amount
    else
      product.amount -= "0.#{self.amount}".to_f
    end

    if product.amount < 0
      raise Error.new 'Quantidade superior ao estoque informado.'
    else
      product.save!
    end
  end
end
