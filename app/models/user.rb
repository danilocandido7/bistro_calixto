class User < ApplicationRecord
  has_secure_password
  validates :name, uniqueness: true, presence: { message: 'Nome não pode ficar em branco' }

  after_destroy :ensure_an_admin_remains

  class Error < StandardError
  end
  
  private

  def ensure_an_admin_remains
    if User.count.zero?
      raise Error.new 'Não é possível deletar o último usuário!'
    end
  end
end
