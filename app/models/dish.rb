class Dish < ApplicationRecord
  paginates_per 15
  
  validates :name,    presence: { message: 'Nome não pode ficar em branco' }
  validates :price,   presence: { message: 'Preço deve ser maior que zero' },
                                  numericality: { greater_than_or_equal_to: 0 }
                 

  has_many :item_dishes
  has_many :products, through: :item_dishes, dependent: :destroy

  accepts_nested_attributes_for :item_dishes

end
