class Product < ApplicationRecord
  paginates_per 15

  MEASURES = [ :kilogramas, :gramas, :litros, :mililitros ].freeze

  validates :name,    presence: { message: 'Nome não pode ficar em branco' }
  validates :measure, presence: { message: 'Medida não pode ficar em branco' }
  validates :brand,   presence: { message: 'Marca não pode ficar em branco' }
  validates :amount,  presence: { message: 'Quantidade não pode ficar em branco' },
                                  numericality: { greater_than_or_equal_to: 0 }

  validate :expiration_date_cannot_be_in_the_past

  has_many :item_dishes
  has_many :dishes, through: :item_dishes, dependent: :destroy

  scope :available_amount, -> { where('amount > 0') }

  def name_with_amount_and_measure
    "#{name} - #{amount} - (#{measure})"
  end

  def self.search(name)
    where('lower(name) like ?', "%#{name}%")
  end

  private

  def expiration_date_cannot_be_in_the_past
    if expiration_date.present? && expiration_date < Date.today
      errors.add(:expiration_date, "Data de expiração não pode estar no passado.")
    end
  end
end
